using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;

public class CompTimeAdjustment : MonoBehaviour
{
    public GameObject fatherObj;
    public List<GameObject> compObjs = new List<GameObject>();
    public List<MediaPlayer> mediaPlayers = new List<MediaPlayer>();

    [Range(0, 20)]
    public List<float> delayTime = new List<float>();
    public float endTime;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < fatherObj.transform.childCount; i++)
        {
            compObjs.Add(fatherObj.transform.GetChild(i).gameObject);
            mediaPlayers.Add(fatherObj.transform.GetChild(i).gameObject.GetComponent<MediaPlayer>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void callVideoPlay()
    {
        for (int i = 0; i < compObjs.Count; i++)
        {
            StartCoroutine(delayAndPlay(delayTime[i], i));
        }
    }
    
    //Wait for time
    IEnumerator delayAndPlay(float second, int i)
    {
        yield return new WaitForSeconds(second);
        mediaPlayers[i].Play();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;
using DG.Tweening;

public class SceneManager : MonoBehaviour
{
    public List<MediaPlayer> Dances = new List<MediaPlayer>();
    public List<MediaPlayer> Idles = new List<MediaPlayer>();
    public List<Light> spotLights = new List<Light>();
    public List<AudioSource> BGMs = new List<AudioSource>();
    public List<bool> Playings = new List<bool>();

    private float lasting = 8f;
    private float speed = 2f;
    private float lightIntensity = 50f;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 5; i++)
        {
            Playings[i] = false;
        }

        StartCoroutine(SwitchEthnicity());

    }

    // Update is called once per frame
    void Update()
    {
        for(int i = 0; i < 5; i++)
        {
            if(Playings[i])
            {
                Dances[i].Play();
                Idles[i].Stop();
                Idles[i].Rewind(true);
            }
            else
            {
                Idles[i].Play();
                Dances[i].Stop();
                Dances[i].Rewind(true);
            }
        }
    }

    IEnumerator SwitchEthnicity()
    {
        for (int i = 0; i < 5; i++)
        {
            Playings[i] = false;
        }

        //Part 0
        Playings[0] = true;
        
        spotLights[0].DOIntensity(lightIntensity, speed);
        
        BGMs[0].Play();
        BGMs[0].DOFade(1f, 1f);

        //Part 1
        yield return new WaitForSeconds(lasting);
        Playings[0] = false;
        Playings[1] = true;
        
        spotLights[0].DOIntensity(0f, speed);
        spotLights[1].DOIntensity(lightIntensity, speed);

        BGMs[0].DOFade(0f, 1f);
        BGMs[1].Play();
        BGMs[1].DOFade(1f, 1f);

        //Part 2
        yield return new WaitForSeconds(lasting);
        Playings[1] = false;
        Playings[2] = true;
        
        spotLights[1].DOIntensity(0f, speed);
        spotLights[2].DOIntensity(lightIntensity, speed);

        BGMs[1].DOFade(0f, 1f);
        BGMs[2].Play();
        BGMs[2].DOFade(1f, 1f);

        //Part 3
        yield return new WaitForSeconds(lasting);
        Playings[2] = false;
        Playings[3] = true;
        
        spotLights[2].DOIntensity(0f, speed);
        spotLights[3].DOIntensity(lightIntensity, speed);

        BGMs[2].DOFade(0f, 1f);
        BGMs[3].Play();
        BGMs[3].DOFade(1f, 1f);

        //Part 4
        yield return new WaitForSeconds(lasting);
        Playings[3] = false;
        Playings[4] = true;
        
        for (int i = 0; i < 4; i++)
        {
            spotLights[i].DOIntensity(lightIntensity, speed);
        }

        BGMs[3].DOFade(0f, 1f);
        BGMs[4].Play();
        BGMs[4].DOFade(1f, 1f);

        //Part 5
        yield return new WaitForSeconds(lasting);
        for (int i = 0; i < 5; i++)
        {
            Playings[i] = false;
            BGMs[i].DOFade(0f, 1f);

            if (i < 4)
            {
                spotLights[i].DOIntensity(0f, speed);
            }
        }

        //Part 6 (replay)
        yield return new WaitForSeconds(3f);
        StartCoroutine(SwitchEthnicity());
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SpotlightPositionAdjustment : MonoBehaviour
{
    public GameObject fatherObj;
    public Material fatherMat;
    public Text instructionText;

    private List<GameObject> childObjs = new List<GameObject>();
    public static bool boolPanelMode = false;
    private int childCount = 0;
    private float speed = 0.05f;

    // Start is called before the first frame update
    void Start()
    {
        // Initialize DOTween
        DOTween.Init(true, true, LogBehaviour.Verbose).SetCapacity(200, 10);

        // Put childObjs into list
        for (int i = 0; i < fatherObj.transform.childCount; i++)
        {
            childObjs.Add(fatherObj.transform.GetChild(i).gameObject);
        }

        // Loading Saved Data
        LoadPreference();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L) && CompPositionAdjustment.boolPanelMode == false)
        {
            // (Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftControl)) && Input.GetKeyDown(KeyCode.L)
            boolPanelMode = !boolPanelMode;

            if (boolPanelMode == false)
            {
                SavePreference();

                fatherMat.DOFade(0f, 2f).SetEase(Ease.InOutSine);
                instructionText.DOFade(0f, 2f).SetEase(Ease.InOutSine);
                for(int i = 0; i < childObjs.Count; i++)
                {
                    childObjs[i].GetComponent<Light>().DOColor(Color.white, 2f).SetEase(Ease.InOutSine);
                }
            }
            else
            {
                fatherMat.DOFade(1f, 2f).SetEase(Ease.InOutSine);
                instructionText.DOFade(1f, 2f).SetEase(Ease.InOutSine);
                for (int i = 0; i < childObjs.Count; i++)
                {
                    childObjs[i].GetComponent<Light>().DOColor(Color.red, 2f).SetEase(Ease.InOutSine);
                }
            }
        }

        if (boolPanelMode)
        {
            //adjust position
            if (Input.GetKeyDown(KeyCode.W))
            {
                childObjs[childCount].transform.position += new Vector3(0, speed, 0);
            }

            else if (Input.GetKeyDown(KeyCode.S))
            {
                childObjs[childCount].transform.position += new Vector3(0, -speed, 0);
            }

            else if (Input.GetKeyDown(KeyCode.A))
            {
                childObjs[childCount].transform.position += new Vector3(-speed, 0, 0);
            }

            else if (Input.GetKeyDown(KeyCode.D))
            {
                childObjs[childCount].transform.position += new Vector3(speed, 0, 0);
            }

            else if (Input.GetKeyDown(KeyCode.Z))
            {
                childObjs[childCount].transform.position += new Vector3(0, 0, -speed);
            }

            else if (Input.GetKeyDown(KeyCode.X))
            {
                childObjs[childCount].transform.position += new Vector3(0, 0, speed);
            }

            //switch the object
            else if (Input.GetKeyDown(KeyCode.B))
            {
                if (childCount > 0)
                {
                    childCount += -1;
                }
                else
                {
                    childCount = childObjs.Count - 1;
                }
            }

            else if (Input.GetKeyDown(KeyCode.N))
            {
                if (childCount < childObjs.Count - 1)
                {
                    childCount += 1;
                }
                else
                {
                    childCount = 0;
                }
            }
        }
    }

    void SavePreference()
    {
        for(int i = 0; i < fatherObj.transform.childCount; i++)
        {
            PlayerPrefs.SetFloat("spotLights[" + i + "]-x", childObjs[i].transform.position.x);
            PlayerPrefs.SetFloat("spotLights[" + i + "]-y", childObjs[i].transform.position.y);
            PlayerPrefs.SetFloat("spotLights[" + i + "]-z", childObjs[i].transform.position.z);
            Debug.Log(PlayerPrefs.GetFloat("spotLights[" + i + "]-x") + "|" + PlayerPrefs.GetFloat("spotLights[" + i + "]-y") + "|" + PlayerPrefs.GetFloat("spotLights[" + i + "]-z"));
        }
    }

    void LoadPreference()
    {
        if (PlayerPrefs.HasKey("spotLights[0]-x") && PlayerPrefs.HasKey("spotLights[0]-y") && PlayerPrefs.HasKey("spotLights[0]-z"))
        {
            Debug.Log("Adjusted");
            for (int i = 0; i < fatherObj.transform.childCount; i++)
            {
                childObjs[i].transform.position = new Vector3(PlayerPrefs.GetFloat("spotLights[" + i + "]-x"), PlayerPrefs.GetFloat("spotLights[" + i + "]-y"), PlayerPrefs.GetFloat("spotLights[" + i + "]-z"));
            }
        }
    }
}
